const {wrap} = require("./synctruck_modules/rwrapper");
const {View} = require("./synctruck_modules/rview");
const {query} = require("./synctruck_modules/rdb");
const {copy} = require("./synctruck_modules/rutils");
const {parse} = require("./synctruck_modules/grammar");
const {Context} = require("./synctruck_modules/rcontext");
const {
        DISPLAY_MODULES, 
        MANUAL_SETTINGS, 
        PAGE_SIZE,
        ALL_STATIONS_MODULES,
        PREVIEW_VERSION,
        EDITION_VERSION
} = require('./synctruck_modules/rschema');

const {preview_action,prepare_edition} = require( "./actions.js" );
const {Response} = require("./Response.js");

const ARGS = 'jkjmvjhrmsgjin';
const R_CLASS = 'R_CLASS';
const R_FUNCTION = 'R_FUNCTION';


async function interpret(token,statement){
    const context = new Context();
    await context.connect(token);
    const compiled_query = parse(statement);

    const limit = ('limit' in compiled_query)?compiled_query['limit'] : PAGE_SIZE;
    const offset = ('offset' in compiled_query)?compiled_query['offset'] : 0;
    let module = context.getModule();
    let version = context.getVersion();
    if ('module' in compiled_query) module = compiled_query['module'];
    if ('version' in compiled_query) version = compiled_query['version'];
    context.update(module,version);
    const v = new View(token,context,statement);
    let id;
    async function fetch_table() {
        const v = new View(token,context,statement);
        return {
            table: await v.query(limit,offset),
            tableSettings:v.getSettings(),
            recordCount: await v.max(),
            module:v.getModule(),
            version:v.getVersion()
        }
    }
    // noinspection FallThroughInSwitchStatementJS
    switch (compiled_query.type) {
        case 'manual-mode':{
            if(version < 700) throw new Error('You must be in Preview or Edition mode before enabling Manual Mode.');
            const mots7 = await fetch_table();  //We are already in preview/edition mode so we just fetch the table as
                                                //usual and change the settings.
            const jk = MANUAL_SETTINGS[module];
            if(!jk) throw new Error('Module: '+module+' is not suitable for Manual Mode.');
            mots7.tableSettings = await jk(token,mots7.tableSettings); //We retrieve the new Settings
            return new Response(mots7,'manual');
        }
        case 'goto': return new Response(await fetch_table());
       case 'change-applicant-status':{
          const id = compiled_query['id'];
          const mots7 = {aux:{}};
          const options = await query('select id,name as code from applicant_action');
           
          mots7.aux[ARGS] = [id,options];
          mots7.aux[R_FUNCTION] = 'change_applicant_status';
          return new Response(mots7,null);   
       }
        case 'list-stations':{
            const stations = await query(`select * from get_stations($1)`,[token]);
            stations.push({id : 'ALL',name:'ALL'});
            const mots7 = {
                available_stations: stations,
                selected_station: {},
                isAuthenticated : true
            };
            mots7.selected_station[ARGS] = [];
            mots7.selected_station[R_FUNCTION] = 'selectFirstStation';
            return new Response(mots7,null); //Don't go anywhere
        }
        case 'list-modules':{
            const modules = await query('select * from get_modules($1)',[token]);
            return new Response({
                available_modules: modules,
                selected_module: modules[0]
            },null); //Don't go anywhere
        }
        case 'update-station' : {
            const target_station = compiled_query['station'];
            if (target_station.toLowerCase()==='all'){
                const all = ALL_STATIONS_MODULES[module];
                if(all) context.update(module,all);
            }else await query('update session set station = $1 where id = $2',[target_station,token]);
            return new Response(await fetch_table());
        }
        case 'logbook':
            return new Response({
                log_book: await query(`select * from ${module.toLowerCase()}_logbook(${compiled_query['id']})`)
            },'logbook');
        case 'update-buttons':{
            const {primary,secondary} = compiled_query;
            await query(`update user_buttons as ub set orden = i.orden, secondary = false
                from (
                    select ${context.usr} as usr,v.code,row_number() over () as orden
                    from (values ${primary.map(p=>`('${p}')`).join(',')}) v(code)
                 ) i where ub.button = i.code and ub.usuario = i.usr`);
            await query(`update user_buttons as ub set orden = i.orden, secondary = true
                    from ( select ${context.usr} as usr,v.code,row_number() over () as orden
                   from (values ${secondary.map(p=>`('${p}')`).join(',')}) v(code) ) i where ub.button = i.code and ub.usuario = i.usr`);
            return new Response({},null);
        }
        case 'profile':
            module = 'USER'; //Remember to change module to PROFILE. As profile should have different configurations than normal User display!
            id = context.usr;
            context.update(module,1);
        case 'display': {
            if(!id) id = compiled_query['id'];
            const base = DISPLAY_MODULES[module];
            if(!base) throw new Error(`Module: ${module} is not suitable for display.`);
            const v = await copy(base,token,id);
            const c_args = [
                v.module,
                v.title,
                v.record,
                v.fields,
                v.configuration,
                v.overview
            ];
            const mots7 = {
                display : {},
                fields : {}
            };
            mots7.display[ARGS] = c_args;
            mots7.display[R_CLASS] = 'R_DISPLAY';

            mots7.fields[ARGS] = [];
            mots7.fields[R_FUNCTION] = 'getFieldGrid';

            return new Response(mots7,'display');
        }
        case 'preview':
        case 'edit':
        {
            let { targets, only } = compiled_query;
            if(targets){
                await prepare_edition(token,module,version,targets,only);
            }
        }
        case 'reset':
        case 'search':
            context.reset();
        case 'filter':
        case 'order':
        case 'fetch':
            v.filter(compiled_query.filter);
            v.order_by(compiled_query.order);
            break;
        case 'prepare':
            await preview_action('prepare',module,version,token,
                {times: compiled_query.times, context : context});
            break;
        case 'update-list':
            let isProfile =  module === 'USER';
            for(const wings of compiled_query['requests']){
                if(isProfile && context.usr === Number(wings.targets[0])) isProfile = true;
                await preview_action('update',module,version,token,
                    {updates:wings.updates,targets:wings.targets, context : context});
            }
            if(isProfile) return new Response({},null,'Updated profile successfully!'); //Don't go anywhere!
            break;
        case 'clone':
            await preview_action('clone',module,version,token,
                {targets:compiled_query.targets,times: compiled_query.times, context : context});
            break;
        case 'delete':
            await preview_action('delete',module,version,token,
                {targets:compiled_query.targets, context : context});
            break;
        case 'fuse':
        case 'cleanse':
        case 'purge':
            await preview_action(compiled_query.type,module,version,token, {context : context, targets: [] });
            break;
    }
    return new Response(await fetch_table());
}

exports.safe_interpret = (token,command) => wrap(interpret,token,command);
exports.interpret = interpret;
