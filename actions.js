const { 
        PREVIEW_IMPLEMENTED_MODULES, 
        RESOLVE_VERSION, 
        PREVIEW_ACTION_MESSAGES,
        PREVIEW_VERSION,
        EDITION_VERSION
}  =  require("./synctruck_modules/rschema");

const {View} = require("./synctruck_modules/rview");
const {query} = require("./synctruck_modules/rdb");

function get_table_name (module,version){
    const t = RESOLVE_VERSION[version];
    if(!t) return module==='USER'?'USUARIO':module.toLowerCase();
    if(!PREVIEW_IMPLEMENTED_MODULES[module])throw `Module: ${module} is not suitable for preview.`;
    return `${module.toLowerCase()}_${t}`
}
function get_performer(module,version,action){
    const t = RESOLVE_VERSION[version];
    if(!t) return '';
    return `${action}_${t}_${module.toLowerCase()}`;
}
/* ----------------------------------------- Preview table standard naming convention -------------------------
All preview tables (Tables NOT rendering function) Have the following name:
Module.toLowercase()_preview.
I.E :   -driver_preview
        -user_preview
Those are tables & are only meant to be used when updating,deleting or inserting data.

The name of the rendering function is different. Those are called:
preview_PREVIEW_POSTFIX
I.E :   -preview_drivers
        -preview_users

Version for ALL preview tables of any module is 776.

All preview implementations come with a series of functions to perform standard operations
like commit,save,duplicate, etc.

Those functions are performed by the Database and are only called from the server.
All of them follow the following convention:

action_PREVIEW_POSTFIX
I.E :   -commit_drivers
        -save_users
        -clone_vans

If you notice, preview also follows this notation. This is because viewing is in certain way considered
as an action as well.

The definition of the view for filtering & rendering purposes is defined in the View file under
the proper module & version.

Therefore, make sure to update or create each and every one of the mentioned functions & definitions
whenever you need to add a new module's preview!
*/

//region FUNCTION IMPLEMENTATION
async function preview_action(action,module,version,session,body){
    const table = get_table_name(module,version);
    let performer = get_performer(module,version,action);
    const { targets, times, updates, context } = body;
    let true_targets ;
    let q;
    let m;
    let v = new View(session,context);
    if(action !== 'update' && version < 700) throw new Error('Invalid operation: '+action+' Make sure you are in preview/edition mode first.');
    if(action!=='prepare'){
        if(targets.length===0) {
            true_targets = `(${v.selectIndexOnly()})`;
        }else{
            true_targets = `(${targets.join(',')})`;
        }
    }
    switch (action) {
        case 'fuse':{
            q = `select * from ${performer}(${session})`;
            m = 'Fused duplicate records successfully!';
            await query(q);
            return {message : m};
        }
        case 'update':
        {
            let true_updates = [];
            for (const u of updates) {
                let k = u.key;
                try{
                    const u2 = v.verify_field(u.key,null); //Verify the column  exists.
                    k = u2[0];
                } catch(err){ } //Ignore.
                /*
                * We take for granted that the key exists. This because a Display may have way too many keys
                * const u2 = v.verify_field(u.key,null); //Verify the column  exists.
                * const k = u2[0];
                * Extract the column name if any. (The 1 index holds the casted value if any.)
                * To verify. Plus it is the task of the Client to verify the existence of each key before making the request.
                * Also, in worst case scenario a raw Postgres error will be returned.
                *
                * Notes:
                * This method was originally intended to be used ONLY for EDIT MODE
                * updates. This means that a few assumptions were correct:
                *  - All fields in a EDIT table are ALWAYS String.
                *  - All rows in EDIT have "indice" as the primary key.
                *  - All fields in an EDIT table are ALWAYS editable. (With the exception of indice ofc)
                *  - There is no need to verify permissions, as the USER wouldn't
                *    even be able to see the EDIT table to begin with if it didn't have
                *    proper permissions.
                *  - The tables to update follow an specific naming convention different
                *    from pure modules.
                *
                * However, since all these assumptions are no longer valid if we attempt to use this same
                * path for Display level updates (Aka direct updates, skipping edit mode updates). I will
                * have to verify the version before applying any update. Permissions will have to be verified as well.
                * If the version is > 700 (aka PREVIEW, EDITION) they are automatically escaped. However,
                * for versions < 700, the values is not escaped. This because the field might numeric or date.
                * */
                //let sg = version<700?u.value:`'${u.value}'`;
                let sg = u.value.toString().trim();
                if(sg === '' || sg === 'null')sg = 'null';
                else if(version > 700) sg = `'${u.value}'`;

                if(k!=='indice' && k!== 'id')true_updates.push({key:k,value:sg});
            }
            if(version < 700){
                true_updates.push({key : 'responsible', value: session});
                //The update is not a sand-box update. The user will attempt to apply a direct update!
                //Therefore, permissions must be checked manually.
                let jm = await query('select * from verify_permissions($1,$2,$3);',[session,'UPDATE',module.toUpperCase()]);
                jm = jm[Object.keys(jm)[0]];
                if(!jm && !(module === 'USER' && context.usr === Number(targets[0])))
                    throw new Error('You do not have permission to modify the '+module+' module');
            }
            if(targets.includes(0)){
                let jk = true_updates.map(v => v.key);
                let jm = true_updates.map(v => v.value);
                q = `insert into ${table}(${jk.join(',')}) values (${jm.join(',')})`;
                m = 'Record created successfully!';
            }else{
                true_updates = true_updates.map(u=>`${u.key} = ${u.value}`);
                q = `update ${table} set ${true_updates.join(',')}`;
                q = version<700? `${q} where id in ${true_targets}`:  `${q} where session = ${session} and indice in ${true_targets}`;
                m = 'Updated records successfully!';
            }
        }
            break;
        case 'clone':{
            m = `Cloned rows successfully!`;
            for (const t of targets) {
                q = `select * from ${performer}(${session},${t},${times})`;
                await query(q);
            }
            return {message: m};
        }
        case 'prepare':
            q = `select * from ${performer}(${session},${times})`;
            m = `Prepared ${times} rows for preview successfully!`;
            break;
        case 'delete':
            q = `delete from ${table} where session = ${session} and indice in ${true_targets}`;
            m = 'Deleted records successfully!';
            break;
        default: {
            m = PREVIEW_ACTION_MESSAGES[action];
            if(!m)throw `Unrecognized action: ${action}`;
            performer += `(${session})`;
            let t = (await query(`select * from ${performer}`))[0];
            const a = t[Object.keys(t)[0]];
            m = `${a?a:''} ${m}`;
            return {message: m}
        }
    }
    await query(q);
    return {message:m};
}
//endregion
async function prepare_edition(session,module,version,targets,only_news = false){
    let true_targets;
    if(targets.length === 0) true_targets = `select jk.id from (${v.selectIDsOnly()}) jk group by  jk.id`;
    else{
        targets = targets.map((t)=>`(${t})`);
        true_targets = `select jk.id from (select jh.id from ( values ${targets.join(',')} ) jh(id)) jk group by  jk.id`;
    }
    const table = get_table_name(module,EDITION_VERSION);
    if(only_news){
        await query(`delete from ${table} where session = ${session}`);
    }
    let q;
    switch (module) {
        case 'DRIVER':
            q = `insert into ${table} (name, phone, driver_license, dl_expiration_date, email, birth_date, station, status, substatus, id,
                         session, indice) select suga.name,suga.phone,suga.driver_license,to_char(suga.dl_expiration_date,'mm/dd/yyyy'),
       suga.email,to_char(suga.birth_date,'mm/dd/yyyy'),suga.station,suga.status,suga.substatus,'SYNC'||jm.id,${session},jm.id from (
        select v.id from (${true_targets}) v left join ( select rm.indice from ${table} rm where rm.session = ${session} ) jin 
        ON jin.indice = v.id where jin.indice is null ) jm inner join ${module.toLowerCase()} suga ON suga.id = jm.id`;
            break;
        default : throw new Error(`Module: ${module} is not suitable for manual mode edition.`) ;
    }
    await query(q);
}
module.exports.preview_action = preview_action;
module.exports.prepare_edition = prepare_edition;
