const { safe_interpret } = require('synctruck-interpreter'); 
var services = require('./commonjs_out/command_grpc_pb');
var messages = require('./commonjs_out/command_pb');
var grpc = require('grpc');

async function interpret(call,callback){
	console.log('Received a call!');
	const token  = call.request.getToken();
	const stmt  = call.request.getStmt();
	const v = await safe_interpret(token,stmt);

	const reply = new messages.CommandResponse();
	reply.setBody(v.body);
	reply.setCode(v.code);
	callback(null,reply);
}

function main() {
  var server = new grpc.Server();
  const port = process.env.GRPC_PORT;
  server.addService(services.InterpreterService, {interpret: interpret});
  server.bind('0.0.0.0:'+port, grpc.ServerCredentials.createInsecure());
  server.start();
  console.log(`Started grpc server at: ${port}`);
}

main();
