// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var command_pb = require('./command_pb.js');

function serialize_pb_Command(arg) {
  if (!(arg instanceof command_pb.Command)) {
    throw new Error('Expected argument of type pb.Command');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_pb_Command(buffer_arg) {
  return command_pb.Command.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_pb_CommandResponse(arg) {
  if (!(arg instanceof command_pb.CommandResponse)) {
    throw new Error('Expected argument of type pb.CommandResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_pb_CommandResponse(buffer_arg) {
  return command_pb.CommandResponse.deserializeBinary(new Uint8Array(buffer_arg));
}


var InterpreterService = exports.InterpreterService = {
  interpret: {
    path: '/pb.Interpreter/Interpret',
    requestStream: false,
    responseStream: false,
    requestType: command_pb.Command,
    responseType: command_pb.CommandResponse,
    requestSerialize: serialize_pb_Command,
    requestDeserialize: deserialize_pb_Command,
    responseSerialize: serialize_pb_CommandResponse,
    responseDeserialize: deserialize_pb_CommandResponse,
  },
};

exports.InterpreterClient = grpc.makeGenericClientConstructor(InterpreterService);
