class Response{
   constructor(data,view = 'search',message) {
      this.data = data; //An Object. The Keys are to be used as replacements
      //to the Context Object in the client.
      this.view = view; //String name of view for the client to go to. Default is Search
      this.message = message; //A message to show to the user. Can be empty.
   }
}
module.exports.Response = Response;
