const {operations} = require("../rschema");

class Filter {
    constructor(body,v) {
        this.view = v;
        this.op = body.op;
        if(!(this.op in operations)) throw `Invalid operation: ${this.op} does not exist.`;
        if((this.op === 'and')||(this.op === 'or')||(this.op === 'not')){
            this.left = new Filter(body.left,v);
            if(body.right)this.right = new Filter(body.right,v); //We check if body.right is defined since the NOT operator doesn't use a right operand!
            else this.right = this.left; //If right is not defined we just clone it.
        }
        this.key = body.key;
        this.value = body.value;
        let aux = this.view.verify_field(this.key,this.value);
        this.key = aux[0];
        this.value = aux[1];
    };
    compile(){
        if(this.op === 'and' || this.op === 'or'|| this.op === 'not') return operations[this.op](this.left,this.right);
        return operations[this.op](this.view,this.key,this.value);
    }
}

module.exports.Filter = Filter;
