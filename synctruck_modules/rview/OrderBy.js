class OrderBy{
    constructor(body,v) {
        if(!body.fields)throw 'You sent an explicit Order By clause. Therefore order by fields are expected and none were received.';
        this.fields = [];
        if(!Array.isArray(body.fields))throw `Array of fields is expected. Got: ${body.fields}`;
        for (const field of body.fields) {
            let aux = v.verify_field(field,null);
            aux = aux[0]; //We dispose of the value.
            this.fields.push(`${v.getAnchor()}.${aux}`);
        }
        this.direction = 'DESC';
        if(body.direction)if(body.direction.toUpperCase()==='ASC')this.direction = 'ASC';
        this.v = v;
    }
    compile(){
        return `${this.fields.join(',')} ${this.direction}`;
    }
}
module.exports.OrderBy = OrderBy;
