const {OrderBy} = require("./OrderBy");
const {Filter} = require("./Filter");
const {next_alias} = require("../rutils");
const {VIEW_TABLES} = require("../rschema");
const {query} = require("../rdb");

class View{
    constructor(session,context,command) {
        this.session = Number(session);
        this.context = context;
        this.command = command;
        this.table = VIEW_TABLES[this.context.getModule()][this.context.getVersion()];
    }
    getStmt(){return this.context.getStmt();}
    getModule(){return this.context.getModule();}
    getVersion(){return this.context.getVersion();}
    getAnchor() {return this.context.anchor;}
    selectIndexOnly() {
        let s = this.context.getStmt();
        let aux = next_alias(s);
        return `select ${aux}.indice from (${s}) ${aux}`;
    }
    selectIDsOnly() {
        let s = this.context.getStmt();
        let aux = next_alias(s);
        return `select ${aux}.id from (${s}) ${aux}`;
    }
    async max(){
        let c = await query(`select count(*) as max from (${this.context.getStmt()}) i`);
        c = c[0];
        return c.max;
    }
    async query(limit,offset){
        limit = Number(limit);
        offset = Number(offset);
        return await query(`${this.context.getStmt()} limit ${limit} offset ${offset}`);
    }
    getSettings(){
        return this.table.settings;
    }
    digest () {
        try { //If insert is unsuccessful, we simply log it silently.
            query('insert into search_history(session,module,version,command,stmt) '
                +'values($1,$2,$3,$4,$5)'
                ,[this.session,this.context.getModule(),this.getVersion(),this.command,this.context.getStmt()]);
        }catch (e) {
            console.log(e);
        }
        return; //It is that simple!!
    };
    filter(filter){
        if(!filter)return;
        this.context.wrap();
        let Jimin = new Filter(filter,this);
        this.context.filter(Jimin.compile());
    }
    order_by(order_by){
        if(!order_by)return;
        this.context.wrap();
        let ob = new OrderBy(order_by,this);
        this.context.order(ob.compile());
    }
    verify_field(key,value){
        if(!key)return [key,value]; //Nothing to do, we return them untouched.
        return this.table.verify_field(key,value);
    }
    getStringKeys(){
        return this.table.getStringKeys();
    }
    is_date_field(k) {
        return this.table.is_date_field(k);
    }
    default_date_field(){
        return this.table.DefaultDateField();
    }
}
module.exports.View = View;
