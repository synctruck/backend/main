const ALIAS_FORMAT = / r[0-9]+ /gm;

const current_alias = function(query){
    return 'r'+(((query || '').match(ALIAS_FORMAT) || []).length);
}
const wrap_query = function(query,anchor){
    return `select * from (${query}) ${anchor} `;
}
const next_alias = function(query){ 
    return 'r'+(((query || '').match(ALIAS_FORMAT) || []).length + 1);
}
const capitalize = function(str){
    str = str.trim();
    str = str.toLowerCase();
    let aux = str.split('_');
    let a = [];
    for (const charElement of aux) {
        a.push(charElement[0].toUpperCase()+charElement.substr(1));
    }
    return a.join(' ');
}

async function copy(){
    let obj = arguments[0];
    let args = [];
    for (let i = 1; i<arguments.length; i++)args.push(arguments[i]);
    if(typeof obj === 'function') return await obj(...args);
    if(typeof obj !== 'object') return obj;
    if(Array.isArray(obj)){
        const jm = [];
        for (const el of obj){
            jm.push( await copy(el,...args));
        }
        return jm;
    }
    const jk = {};
    for(const key in obj){
        switch(typeof obj[key]){
            case 'string' :
            case 'number':
            case 'boolean': jk[key] = obj[key]; break;
            case 'object': jk[key] = await copy(obj[key],...args); break;
            case 'function': jk[key] = await obj[key](...args); break;
            default: throw new Error('Unrecognized type: '+(typeof obj[key])+' for key: '+key+' failed to clone obj.');
        }
    }
    return jk;
}

exports.copy = copy;
exports.current_alias = current_alias;
exports.next_alias = next_alias;
exports.wrap_query = wrap_query;
exports.capitalize = capitalize;
