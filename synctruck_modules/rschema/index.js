const { query } = require("../rdb");
const { RTable } = require("./models/RTable");
const { Setting } = require("./models/Setting");
const { KeyDescription } = require("./models/KeyDescription");
const { 
        PAGE_SIZE,
        MODULE_ALIASES,
        DISPLAY_FORMAT,
        PREVIEW_VERSION,
        EDITION_VERSION,
        RESOLVE_VERSION,
        DATE_FORMAT,
        PREVIEW_IMPLEMENTED_MODULES,
        PREVIEW_ACTION_MESSAGES,
        ALL_STATIONS_MODULES,
        operations
} = require("./models/Constants.js");

const MONTH_LITERALS = {
    'jan': 1,
    'feb': 2,
    'mar': 3,
    'apr': 4,
    'may': 5,
    'jun': 6,
    'jul': 7,
    'aug': 8,
    'sep': 9,
    'oct': 10,
    'nov': 11,
    'dec': 12
};

/* Remember: To implement a new module, declare it's tables here. If displayable implement
* its Display here. In display you can indicate whether it is loggable, has access to files, etc.
* However, besides implementing them here, remember to:
* Implement its base table in the DB (obviously)
* Implement at least 1 function_view for the default version of the module.
* Implement preview & edit function in DB if required.
* Implement display function in DB if applicable.
* Implement Logbook trigger & create logbook table if applicable.
* Implement Files Table & relation if applicable.
* Register the Module in the Modules table.
* Register all available tables and their versions in the DB.
* Register one alias to resolve the module & version during parsing. You can
* so in the models.js file in Grammar. This is VERY important, otherwise you
* won't be able to query the module without explicit module name and version! */


const RECURRENT_VALUES = {
    APPLICANT : {
        KEYS : [
            new KeyDescription('date_added','date',['added','timestamp','submitted_date','submit_date']),
            new KeyDescription('action','string',['status']),
            new KeyDescription('name','string',['full_name']),
            new KeyDescription('phone','string',['phone_number']),
            new KeyDescription('email','string',['email_address']),
            new KeyDescription('station','string',['ciudad','city']),
            new KeyDescription('job_posting','string',['job','ad','publicity']),
            new KeyDescription('is_old_enough','string',['old_enough']),
            new KeyDescription('has_valid_license','string',['valid_license','license']),
            new KeyDescription('background_check','string',['drugs','check','willing_to_check']),
            new KeyDescription('is_strong_enough','string',['strong','strong_enough']),
            new KeyDescription('resume','string',['cv','document'])
        ],
        SETTINGS : [
            new Setting('name','Full Name'),
            new Setting('action','Status',{link : 'action_command'}),
            new Setting('date_added_display','Date Added'),
            new Setting('email','Email'),
            new Setting('phone','Phone Number'),
            new Setting('station','Station'),
            new Setting('job_posting','Where did he see our Job Posting?'),
            new Setting('is_old_enough','Is 23 years of age or older?'),
            new Setting('has_valid_license',"Has a valid US driver's license?"),
            new Setting('background_check','Is willing to submit to a background check and drug test?'),
            new Setting('is_strong_enough','Is able to lift packages up to 50lbs?'),
            new Setting('resume_display','Resume',{link : 'resume'})
        ]
    }
};

const VIEW_TABLES = {
    'APPLICANT':{
        1: new RTable('applicants_1')
            .setKeys(RECURRENT_VALUES.APPLICANT.KEYS)
            .setSettings(RECURRENT_VALUES.APPLICANT.SETTINGS),
        2: new RTable('applicants_2')
            .setKeys(RECURRENT_VALUES.APPLICANT.KEYS)
            .setSettings(RECURRENT_VALUES.APPLICANT.SETTINGS),
        3: new RTable('applicants_3')
            .setKeys(RECURRENT_VALUES.APPLICANT.KEYS)
            .setSettings(RECURRENT_VALUES.APPLICANT.SETTINGS),
        4: new RTable('applicants_4')
            .setKeys(RECURRENT_VALUES.APPLICANT.KEYS)
            .setSettings(RECURRENT_VALUES.APPLICANT.SETTINGS),
        5: new RTable('applicants_5')
            .setKeys(RECURRENT_VALUES.APPLICANT.KEYS)
            .setSettings(RECURRENT_VALUES.APPLICANT.SETTINGS)
    },
    'DRIVER' : {
        1 : new RTable('drivers_1').setKeys([
            new KeyDescription('id','number',['code']),
            new KeyDescription('date_added','date',['added']),
            new KeyDescription('name','string',['full_name']),
            new KeyDescription('email','string'),
            new KeyDescription('age','number'),
            new KeyDescription('driver_license','string',['license']),
            new KeyDescription('phone','string',['phone_number']),
            new KeyDescription('status','string'),
            new KeyDescription('substatus','string')
        ]).setSettings([
            new Setting('id_display','ID'),
            new Setting('name','Full Name',{link:'default'}),
            new Setting('date_added_display','Date Added'),
            new Setting('age','Age (years)'),
            new Setting('email','Email'),
            new Setting('phone','Phone Number'),
            new Setting('status','Status',{cell_color:'status_color'}),
            new Setting('substatus','Substatus',{cell_color:'status_color'}),
            new Setting('driver_license','Driver License')
        ]),
        2 : new RTable('drivers_2').setKeys([
            new KeyDescription('id','number',['code']),
            new KeyDescription('date_added','date',['added']),
            new KeyDescription('name','string',['full_name']),
            new KeyDescription('email','string'),
            new KeyDescription('age','number'),
            new KeyDescription('driver_license','string',['license']),
            new KeyDescription('phone','string',['phone_number']),
            new KeyDescription('status','string'),
            new KeyDescription('substatus','string'),
            new KeyDescription('station','string',['station_code'])
        ]).setSettings([
            new Setting('id_display','ID'),
            new Setting('station','Station'),
            new Setting('name','Full Name',{link:'default'}),
            new Setting('date_added_display','Date Added'),
            new Setting('age','Age (years)'),
            new Setting('email','Email'),
            new Setting('phone','Phone Number'),
            new Setting('status','Status',{cell_color:'status_color'}),
            new Setting('substatus','Substatus',{cell_color:'status_color'}),
            new Setting('driver_license','Driver License')
        ]),
        3 : new RTable('drivers_3').setKeys([
            new KeyDescription('id','number',['code']),
            new KeyDescription('name','string',['full_name']),
            new KeyDescription('station','string',['station_code']),
            new KeyDescription('phone','string',['phone_number']),
            new KeyDescription('until_dl_expires','string',['time_for_dl_to_expire','remaining_time']),
            new KeyDescription('driver_license','string',['license']),
            new KeyDescription('color','string',['tier']),
            new KeyDescription('dl_expiration_date','date',['expiration_date']),
        ]).setSettings([
            new Setting('id_display','ID'),
            new Setting('station','Station'),
            new Setting('name','Full Name',{link:'default'}),
            new Setting('driver_license','Driver License'),
            new Setting('until_dl_expires','Until DL Expires',{cell_color:'color'}),
            new Setting('dl_expiration_date_display','DL Expiration Date'),
            new Setting('phone','Phone Number')
        ]),
        776 : new RTable('preview_driver').setKeys([
            new KeyDescription('indice','number',['code','id','index']),
            new KeyDescription('color','string',['row_color']),
            new KeyDescription('name','string',['full_name']),
            new KeyDescription('phone','string',['phone_number']),
            new KeyDescription('driver_license','string',['license']),
            new KeyDescription('dl_expiration_date','string',['driver_license_expiration_date']),
            new KeyDescription('email','string'),
            new KeyDescription('birth_date','string',['birth']),
            new KeyDescription('status','string'),
            new KeyDescription('substatus','string',['sub_status']),
            new KeyDescription('station','string',['station_code'])
        ]).setSettings([
            new Setting('indice','Index',{background_color:'color'}),
            new Setting('station','Station',{background_color:'color',cell_color:'station_color'}),
            new Setting('name','Full Name',{background_color:'color'}),
            new Setting('email','Email',{background_color:'color'}),
            new Setting('phone','Phone Number',{background_color:'color'}),
            new Setting('status','Status',{cell_color:'status_color',background_color:'color'}),
            new Setting('substatus','Substatus',{cell_color:'substatus_color',background_color:'color'}),
            new Setting('birth_date','Birth Date',{cell_color:'birth_date_color',background_color:'color'}),
            new Setting('dl_expiration_date','DL Expiration Date',{cell_color:'dl_expiration_color',background_color:'color'}),
            new Setting('driver_license','Driver License',{background_color:'color'})
        ]),
        777 : new RTable('edit_driver').setKeys([
            new KeyDescription('indice','number',['code','id','index']),
            new KeyDescription('color','string',['row_color']),
            new KeyDescription('name','string',['full_name']),
            new KeyDescription('phone','string',['phone_number']),
            new KeyDescription('driver_license','string',['license']),
            new KeyDescription('dl_expiration_date','string',['driver_license_expiration_date']),
            new KeyDescription('email','string'),
            new KeyDescription('birth_date','string',['birth']),
            new KeyDescription('status','string'),
            new KeyDescription('substatus','string',['sub_status']),
            new KeyDescription('station','string',['station_code'])
        ]).setSettings([
            new Setting('id','ID',{background_color:'color'}),
            new Setting('station','Station',{background_color:'color',cell_color:'station_color'}),
            new Setting('name','Full Name',{background_color:'color'}),
            new Setting('email','Email',{background_color:'color'}),
            new Setting('phone','Phone Number',{background_color:'color'}),
            new Setting('status','Status',{cell_color:'status_color',background_color:'color'}),
            new Setting('substatus','Substatus',{cell_color:'substatus_color',background_color:'color'}),
            new Setting('birth_date','Birth Date',{cell_color:'birth_date_color',background_color:'color'}),
            new Setting('dl_expiration_date','DL Expiration Date',{cell_color:'dl_expiration_color',background_color:'color'}),
            new Setting('driver_license','Driver License',{background_color:'color'})
        ]),
        775 : new RTable('driver',true).setKeys([
            new KeyDescription('name','string',['full_name']),
            new KeyDescription('phone','string',['phone_number']),
            new KeyDescription('driver_license','string',['license']),
            new KeyDescription('dl_expiration_date','date',['expiration_date']),
            new KeyDescription('email','string'),
            new KeyDescription('birth_date','date',['birth']),
            new KeyDescription('station','string',['station_code']),
            new KeyDescription('status','string'),
            new KeyDescription('substatus','string')
        ])
    },
    'HELP':{
        1: new RTable('help_1').setKeys([
            new KeyDescription('module','string'),
            new KeyDescription('command','string'),
            new KeyDescription('alias','string'),
            new KeyDescription('description','string')
        ]).setSettings([
            new Setting('module','Module'),
            new Setting('command','Command'),
            new Setting('alias','Table Name'),
            new Setting('description','Description')
        ])
    },
    'HISTORY' : {
        1 : new RTable('history_1').setKeys([
            new KeyDescription('performed_at','date',['performed']),
            new KeyDescription('module','string'),
            new KeyDescription('version','number'),
            new KeyDescription('command','string')
        ]).setSettings([
            new Setting('performed_at_display','Performed At'),
            new Setting('module','Module'),
            new Setting('version','Version'),
            new Setting('command','Command')
        ]),
        2:  new RTable('history_4').setKeys([
            new KeyDescription('performed_at','date',['performed']),
            new KeyDescription('module','string'),
            new KeyDescription('command','string'),
            new KeyDescription('action','string')
        ]).setDefaultSettings()
    },
    'POSITION':{
        1:new RTable('positions_1').setKeys([
            new KeyDescription('name','string'),
            new KeyDescription('modules','string'),
            new KeyDescription('users','number')
        ]).setSettings([
            new Setting('name','Name'),
            new Setting('modules','Modules'),
            new Setting('users','Users')
        ]),
        2:new RTable('positions_2').setKeys([
            new KeyDescription('name','string'),
            new KeyDescription('module','string'),
            new KeyDescription('action','string')
        ]).setSettings([
            new Setting('name','Name'),
            new Setting('module','Module'),
            new Setting('action','Action')
        ])
    },
    'STATION':{
        1:new RTable('stations_1').setKeys([
            new KeyDescription('id','number'),
            new KeyDescription('code','string'),
            new KeyDescription('state','string'),
            new KeyDescription('city','string'),
            new KeyDescription('address','string'),
            new KeyDescription('phone','string',['phone_number']),
            new KeyDescription('users','number'),
            new KeyDescription('drivers','number'),
            new KeyDescription('vans','number')
        ]).setSettings([
            new Setting('code','Code'),
            new Setting('state','State'),
            new Setting('city','City'),
            new Setting('address','Address'),
            new Setting('phone','Phone'),
            new Setting('users','Users'),
            new Setting('drivers','Drivers'),
            new Setting('vans','Vans')
        ]),
        775 : new RTable('station').setKeys([
            new KeyDescription('address','string'),
            new KeyDescription('city','string'),
            new KeyDescription('state','string'),
            new KeyDescription('phone','string',['phone_number'])
        ])
    },
    'USER' : {
        1 : new RTable('users_1').setKeys([
            new KeyDescription('id','number',['code']),
            new KeyDescription('date_added','date',['added']),
            new KeyDescription('rol','string',['position']),
            new KeyDescription('email','string'),
            new KeyDescription('status','string'),
            new KeyDescription('name','string',['full_name']),
            new KeyDescription('username','string'),
            new KeyDescription('phone','string',['phone_number']),
            new KeyDescription('stations','string'),
            new KeyDescription('last_login','string')
        ]).setSettings([
            new Setting('id_display','ID'),
            new Setting('date_added_display','Date Added'),
            new Setting('name','Full Name'),
            new Setting('rol','Position'),
            new Setting('status','Status'),
            new Setting('last_login','Last login'),
            new Setting('stations','Stations'),
            new Setting('phone','Phone Number'),
            new Setting('email','Email')
        ]),
        2 : new RTable('users_2').setKeys([
            new KeyDescription('id','number',['code']),
            new KeyDescription('date_added','date',['added']),
            new KeyDescription('rol','string',['position']),
            new KeyDescription('email','string'),
            new KeyDescription('status','string'),
            new KeyDescription('name','string',['full_name']),
            new KeyDescription('username','string'),
            new KeyDescription('phone','string',['phone_number']),
            new KeyDescription('stations','string'),
            new KeyDescription('last_login','string')
        ]).setSettings([
            new Setting('id_display','ID'),
            new Setting('date_added_display','Date Added'),
            new Setting('name','Full Name'),
            new Setting('rol','Rol'),
            new Setting('status','Status'),
            new Setting('last_login','Last login'),
            new Setting('stations','Stations'),
            new Setting('phone','Phone'),
            new Setting('email','Email')
        ]),
        3 : new RTable('users_3').setKeys([
            new KeyDescription('id','number',['code']),
            new KeyDescription('name','string',['full_name']),
            new KeyDescription('module','string')
        ]).setSettings([
            new Setting('id_display','ID'),
            new Setting('name','Full Name'),
            new Setting('module','Module')
        ]) ,
        4 : new RTable('users_4').setKeys([
            new KeyDescription('id','number',['code']),
            new KeyDescription('name','string',['full_name']),
            new KeyDescription('module','string'),
            new KeyDescription('action','string')
        ]).setSettings([
            new Setting('id_display','ID'),
            new Setting('name','Full Name'),
            new Setting('module','Module'),
            new Setting('action','Action')
        ]) ,
        5 : new RTable('users_5').setKeys([
            new KeyDescription('code','string',['action']),
            new KeyDescription('description','string')
        ]).setSettings([
            new Setting('code','Action'),
            new Setting('description','Description')
        ]),
        775 : new RTable('usuario',true).setKeys([
            new KeyDescription('name','string',['full_name']),
            new KeyDescription('address','string',['direction']),
            new KeyDescription('phone','string',['phone_number']),
            new KeyDescription('alt_phone','string',['alt_phone_number','alt','alternative_phone']),
            new KeyDescription('email','string'),
            new KeyDescription('birth_date','date',['birth']),
            new KeyDescription('rol','string',['position']),
            new KeyDescription('username','string'),
            new KeyDescription('password','string')
        ])
    },
    'VAN' :{
        775 : new RTable('van',true)
    }
};

const MANUAL_SETTINGS = {
    /*
    * Manual mode is basically the same as a "normal" mode fetch table. However, besides changing
    * the name of the base table to query, a Manual Mode fetch also holds special Table Settings.
    * This is because Manual Mode Settings will usually include options to be used as select boxes,
    * have special colors or input types. However, this is the only change between normal and manual
    * mode table fetch. Therefore, to retrieve a manual mode fetch request, is as simple as returning
    * a normal mode fetch and changing the settings. Since settings are heavily influenced by the
    * original settings it is faster and simpler to define a function that "digests" the original
    * settings and modifies them instead of re-defining the settings.
    *
    * TLDR: This Object contains a series of functions with the signature (token,settings) that
    * returns a new set of settings appropiate for Manual mode. Usually this consists of minimal
    * changes related to the type of field (select,label,input,etc) And suggestions.
    * */
    'DRIVER' : async (session,settings) => {
        settings = settings.map(jk => new Setting(jk.primaryKey,jk.header,jk));
        const neo_settings = [];
       for(const s of settings){
           switch (s.primaryKey){
               case 'station' : {
                   s['cell_type'] = 'select';
                   s['options'] = (await query('select * from get_stations($1)',[session])).map((s=>s.id));
                   break;
               }
               case 'status' : {
                   s['cell_type'] = 'select';
                   s['options'] = (await query('select * from status')).map((s)=>s.code);
                   break;
               }
               case 'substatus' : {
                   s['cell_type'] = 'select';
                   s['options'] = (await query('select * from sub_status')).map((ss)=>ss.code);
                   break;
               }
               case 'indice':
               case 'id':
               case 'index': s['cell_type'] = 'label'; break;
               default:
                   s['cell_type'] = 'input';
           }
           neo_settings.push(s);
       }
       return neo_settings;
    }
};

const DISPLAY_MODULES = {
    'USER' : {
        module:'USER',
        title:'name',
        record : async (session,id) => (await query('select * from display_user($1,$2)',[session,id]))[0]['display_user'],
        fields : [
            {key:'display_id',name:'ID',editable:false},
            {key:'name',name:'Full Name',optional:false},
            {key:'username',name:'Username',editable: false},
            {key:'password',name:'Password',optional:false, field_type:'password'},
            {key:'rol',name:'Position',editable: false},
            {key:'email',name:'Email',optional:false},
            {key:'phone',name:'Phone Number',optional:true},
            {key:'alt_phone',name:'Alt. Phone',optional:true},
            {key:'birth_date',name:'Birth Date',optional:true,type:'date'},
            {key:'date_added',name:'Registered Date',editable: false,type: 'date'},
            {key:'stations',name:'Assigned Stations',editable: false}
        ],
        configuration: {
            history : false,
            files : false,
            style : {}
        },
        overview : [
            {key:'id',name:'ID'},
            {key:'name',name:'Full Name'},
            {key:'rol',name:'Position'},
            {key:'username',name:'Username'},
            {key:'phone',name:'Phone Number'}
        ]
    },
    'DRIVER' : {
        module:'DRIVER',
        title:'name',
        record : async (session,id) => (await query('select * from display_driver($1,$2)',[session,id]))[0],
        fields : [
            {key:'name',name:'Full Name',optional:false},
            {key:'id',name:'ID',editable:false},
            {key:'date_added',name:'Date Added',editable:false},
            {key:'phone',name:'Phone Number',optional:false},
            {key:'driver_license',name:'Driver License',optional:false},
            {key:'dl_expiration_date',name:'DL Expiration date',optional:true,type:'date'},
            {key:'email',name:'Email',optional:false},
            {key:'birth_date',name:'Birth Date',optional:true,type:'date'},
            {
                key:'station',name:'Station',optional:false, field_type:'select',
                options: async (session)  => (await query('select * from get_stations($1)',[session])).map(s=>s.id)
            },
            {
                key:'status',name:'Status',optional:true,field_type: 'select',
                options: async () => (await query('select * from status')).map(s=>s.code)
            },
            {
                key:'substatus',name:'Substatus',optional:true,field_type: 'select',variable_key:'status',
                variable_options: async () => {
                    let substatus = await query(`select code as status, "subStatusCode" as substatus 
                    from status left join status_substatuses_sub_status ssss on status.code = ssss."statusCode" order by code asc;`);
                    let true_substatus = {};
                    for (const entry of substatus) {
                        if(entry.status in true_substatus)true_substatus[entry.status].push(entry.substatus);
                        else true_substatus[entry.status] = [entry.substatus];
                    }
                    return true_substatus;
                }
            }
        ],
        configuration: {
            history : true,
            files : true,
            style : {
                status:{cell_color : 'status_color'},
                substatus : {cell_color : 'status_color'}
            }
        },
        overview : [
            {key:'id',name:'ID'},
            {key:'name',name:'Full Name'},
            {key:'station',name:'Station'},
            {key:'email',name:'Email'},
            {key:'status',name:'Status'},
            {key:'substatus',name:'Substatus'}
        ]
    }
};

const TABLE_MAPS = {
    'applicants':{
      module : 'APPLICANT',
      version : 1
    },
    'repeated_applicants':{
        module : 'APPLICANT',
        version : 2
    },
    'approved_applicants':{
        module : 'APPLICANT',
        version : 3
    },
    'rejected_applicants':{
        module : 'APPLICANT',
        version : 5
    },
    'all_applicants':{
        module : 'APPLICANT',
        version : 4
    },
    'expiring_dl' : {
        module : 'DRIVER',
        version : 3
    },
    'drivers' : {
        module:'DRIVER',
        version: 1
    },
    'help':{
        module:'HELP',
        version : 1
    },
    'quick_access':{
        module:'HELP',
        version : 1
    },
    'users':{
        module:"USER",
        version : 1
    },
    'user_access':{
        module:"USER",
        version:3
    },
    'stations':{
        module:"STATION",
        version:1
    },
    'positions':{
        module:"POSITION",
        version:1
    },
    'history':{
        module:"HISTORY",
        version:1
    },
    'all_history':{
        module:"HISTORY",
        version:2
    },
    'explicit_user_access':{
        module:"USER",
        version:4
    },
    'explicit_positions':{
        module:"POSITION",
        version:2
    },
    'users_per_station':{
        module:"USER",
        version:2
    },
    'all_drivers':{
        module:"DRIVER",
        version:2
    },
    'actions':{
        module:"USER",
        version:5
    }
};

exports.VIEW_TABLES = VIEW_TABLES;
exports.DISPLAY_MODULES = DISPLAY_MODULES;
exports.MANUAL_SETTINGS = MANUAL_SETTINGS;

exports.MONTH_LITERALS = MONTH_LITERALS;
exports.PAGE_SIZE = PAGE_SIZE;
exports.MODULE_ALIASES = MODULE_ALIASES;
exports.DISPLAY_FORMAT = DISPLAY_FORMAT;
exports.PREVIEW_VERSION = PREVIEW_VERSION;
exports.EDITION_VERSION = EDITION_VERSION;
exports.RESOLVE_VERSION = RESOLVE_VERSION;
exports.DATE_FORMAT = DATE_FORMAT;
exports.PREVIEW_IMPLEMENTED_MODULES = PREVIEW_IMPLEMENTED_MODULES;
exports.PREVIEW_ACTION_MESSAGES = PREVIEW_ACTION_MESSAGES;
exports.ALL_STATIONS_MODULES = ALL_STATIONS_MODULES;
exports.TABLE_MAPS = TABLE_MAPS;
exports.operations = operations;
