const {DATE_FORMAT} = require("./Constants");

class KeyDescription {
    constructor(name,type,alt_keys) {
        this.name = name;
        this.type = type;
        if(alt_keys)this.alt_keys = alt_keys;
        else this.alt_keys = [];
    }

    match_key(key) {
        if(this.name === key)return this.name;
        for (const keyElement of this.alt_keys) {
            if(keyElement === key)return this.name;
        }
        return undefined;
    }

    match_value(value) {
        //The key does exist! However, we must still verify a few things more:
        if(!value)return value; //If value is not defined, that is all!
        if(Array.isArray(value)){//Check if array is homogenous:
            if(value.length === 0)return []; //Empty array, might or might no be accepted.
            let t = typeof value[0];
            let v = [];
            for (const e of value) {
                if(t !== typeof e)throw `Array of values must be homogenous. Expected:${t} Got: ${typeof e}. Full array: ${value.join(',')}`;
                if(this.type==='string')v.push((e[0]==="'")?e:`'${e}'`);
                else if(this.type ==='number')v.push(Number(e));
                else v.push(e);
            }
            value  = v;
            if(this.type === 'date'){
                if(!DATE_FORMAT.test(value[0]))throw `Date Array expected for field: ${this.name}. Got: ${value.join(',')}`;
                return value; //All is fine.
            }
            if(this.type === 'number'){
                if(isNaN(value[0]))throw `Number array expected. for field: ${this.name}. Got: ${value.join(',')}`;
                return value; //All is fine.
            }
            if(this.type === 'string')return value; //Anything can pass as string.
            throw `Unexpected type for elements of array: ${value.join(',')}`;
        }
        if(this.type === 'date'){
            if(!DATE_FORMAT.test(value))throw `Date type expected for field: ${this.name}. Got: ${value}`;
            return value; //It passed the test!
        }
        if(this.type==='number'){
            let j = Number(value)
            if(isNaN(j))throw `Number expected for field: ${this.name}. Got: ${value}`;
            return Number(value); //We make sure it is a number!
        }
        if(this.type==='string'){
            if(value[0]!=="'")return `'${value}'`; //We wrap it in quotes if not already wrapped!!
            else return value;
        }
        throw `Unexpected type for field: ${this.name}. Value: ${value}`;
    }
}
module.exports.KeyDescription = KeyDescription;
