class Setting {
    constructor(primaryKey,header,extras) {
        this.primaryKey = primaryKey;
        this.header = header;
        if(extras){
            for (const extra in extras) {
                this[extra] = extras[extra];
            }
            return;
        }
        if(primaryKey==="command"){
            this.cell_link = 'command';
        }
    }
}
module.exports.Setting = Setting;