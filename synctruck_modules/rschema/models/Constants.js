const DATE_FORMAT = /to_date\([^)]+\)/;
const DISPLAY_FORMAT = /[^_ \n]+_display/;
const PREVIEW_VERSION = 776;
const EDITION_VERSION = 777;
const RESOLVE_VERSION = {776 : 'preview', 777: 'edit'};
const PREVIEW_IMPLEMENTED_MODULES = {'DRIVER': true};

const ALL_STATIONS_MODULES = {
    'DRIVER' : 2,
    'APPLICANT' : 4
}; //Not all modules have an ALL STATIONS version.
//If a module has an ALL STATIONS version it will be mapped here.
//When a user picks all stations within a module that does not
//support an All stations version, nothing will happen.
const PREVIEW_ACTION_MESSAGES = {
    commit : ' records committed successfully.',
    save : ' records saved successfully.',
    purge : ' records deleted successfully.',
    cleanse : 'records removed successfully.',
    fuse : 'duplicate records fused successfully.'
};
const MODULE_ALIASES = {
    'DRIVERS' : 'DRIVER',
    'VANS' : 'VAN',
    'USERS' : 'USER',
    'APPLICANTS' : 'APPLICANT',
    'ASPIRANT' : 'APPLICANT',
    'ASPIRANTS' : 'APPLICANT'
};
const operations =  {
    'in' : function(view,key,value){
        return `(${view.getAnchor()}.${key} in (${value.join(',')}))`;
    },
    '<' : function(view,key,value){
        return `(${view.getAnchor()}.${key} < ${value})`;
    },
    '>': function(view, key,value){
        return `(${view.getAnchor()}.${key} > ${value})`;
    },
    '<=' : function(view,key,value){
        return `(${view.getAnchor()}.${key} <= ${value})`;
    },
    '>=' : function(view, key,value){
        return `(${view.getAnchor()}.${key} >= ${value})`;
    },
    '=' : function(view,key,value){
        return `(${view.getAnchor()}.${key} = ${value})`;
    },
    '!=' : function(view,key,value){
        return `(${view.getAnchor()}.${key} != ${value})`;
    },
    'from_to': function(view,key,value){
        if(!key)key = view.default_date_field();
        if(!view.is_date_field(key))throw `Field: ${key} is not a date field.`;
        const lower_limit = value[0];
        const upper_limit = value[1];
        return `(${view.getAnchor()}.${key} >= ${lower_limit} AND ${view.getAnchor()}.${key} <= ${upper_limit})`;
    },
    'contains': function(view,key,value){
        if(value[0]==="'")value = value.substr(1,value.length-2); //We remove the escaped quotes.
        value = `'%${value.trim()}%'`;
        if(!key){
            let keys = view.getStringKeys();
            let aux = [];
            for (const k of keys) {
                aux.push(`${view.getAnchor()}.${k} ILIKE ${value}`);
            }
            return `(${aux.join(' or ')})`;
        }
        return `(${view.getAnchor()}.${key} ILIKE ${value})`;
    },
    'starts_with': function(view,key,value){
        let ren;
        if(!key) ren = view.getStringKeys();
        else ren = [key];
        let result = [];
        for (const key of ren) {
            if(typeof value === "string"){
                if(value[0]==="'")value = value.substr(1,value.length-2); //We remove the escaped quotes.
                let t = `(${view.getAnchor()}.${key} ILIKE '${value}%')`;
                result.push(t);
                continue;
            }else if(Array.isArray(value)){
                let aux = [];
                for (const v of value) {
                    let l = v;
                    if(l[0]==="'")l = l.substr(1,l.length-2); //We remove the escaped quotes.
                    let t = `${view.getAnchor()}.${key} ILIKE '${l}%'`;
                    aux.push(t);
                }
                let tt = `(${aux.join(' or ')})`;
                result.push(tt);
                continue;
            }
            throw `Starts with operator can only be performed on an array of strings or a string. Got: ${value}`;
        }
        return result.join(' or ');
    },
    'ends_with': function(view,key,value){
        let ren;
        if(!key) ren = view.getStringKeys();
        else ren = [key];
        let result = [];
        for (const key of ren) {
            if(typeof value === "string"){
                if(value[0]==="'")value = value.substr(1,value.length-2); //We remove the scaped quotes.
                let t = ` (${view.getAnchor()}.${key} ILIKE '%${value}' ) `;
                result.push(t);
                continue;
            }else if(Array.isArray(value)){
                let aux = [];
                for (const v of value) {
                    let l = v;
                    if(l[0]==="'")l = l.substr(1,l.length-2); //We remove the scaped quotes.
                    let tt = `${view.getAnchor()}.${key} ILIKE '%${l}'`;
                    aux.push(tt);
                }
                let ttt = `(${aux.join(' or ')})`;
                result.push(ttt);
                continue;
            }
            throw `Ends with operator can only be performed on an array of strings or a string. Got: ${value}`;
        }
        return result.join(' or ');
    },
    'from': function(view,key,value){
        if(!key)key = view.default_date_field();
        if(!view.is_date_field(key))throw `Field: ${key} is not a date field.`;
        if(!(typeof value === "string"))throw `from operator expects a date. Got: ${value}`;
        return `(${view.getAnchor()}.${key} >= ${value})`;
    },
    'is_null': function(view,key,value){
        let aux = view.verify_field(key,null);
        aux = aux[0]; //We just resolved the alias if any.
        return `(${view.getAnchor()}.${aux} is NULL)`;
    },
    'is_not_null': function(view,key,value){
        let aux = view.verify_field(key,null);
        aux = aux[0]; //We just resolved the alias if any.
        return `(${view.getAnchor()}.${aux} IS NOT NULL)`;
    },
    'and': function(left,right){
        return `(${left.compile()} AND ${right.compile()})`;
    },
    'or' :  function(left,right){
        return `(${left.compile()} OR ${right.compile()})`;
    },
    'not':function(left,right){
        return `NOT (${left.compile()})`;
    }
};
const PAGE_SIZE = 25;

module.exports.PAGE_SIZE = PAGE_SIZE;
module.exports.MODULE_ALIASES = MODULE_ALIASES;
module.exports.DISPLAY_FORMAT = DISPLAY_FORMAT;
module.exports.PREVIEW_VERSION = PREVIEW_VERSION;
module.exports.EDITION_VERSION = EDITION_VERSION;
module.exports.RESOLVE_VERSION = RESOLVE_VERSION;
module.exports.DATE_FORMAT = DATE_FORMAT;
module.exports.operations = operations;
module.exports.PREVIEW_IMPLEMENTED_MODULES = PREVIEW_IMPLEMENTED_MODULES;
module.exports.PREVIEW_ACTION_MESSAGES = PREVIEW_ACTION_MESSAGES;
module.exports.ALL_STATIONS_MODULES = ALL_STATIONS_MODULES;
