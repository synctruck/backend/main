const {capitalize} = require("../../rutils");
const {Setting} = require("./Setting");

class RTable {
    constructor(name,loggable) {
        this.name = name;
        this.keys = [];
        this.loggable = loggable;
        this.settings = [];
    }
    setKeys(k){
        this.keys = k;
        return this;
    }
    setSettings(s){
        this.settings = s;
        return this;
    }
    verify_field(key, value){
        for (const k of this.keys) {
            let true_key = k.match_key(key);
            if(true_key){
                return [true_key,k.match_value(value)];
            }
        }
        throw `Invalid field within filter expression: ${key} does not exist.`;
    }

    getStringKeys() {
        let r = [];
        for (const stmtKey of this.keys) {
            if(stmtKey.type === 'string')r.push(stmtKey.name);
        }
        if (r.length===0)throw `FATAL ERROR! NO STRING FIELDS FOUND WITHIN VIEW: ${this.name}.`;
        return r;
    }

    is_date_field(k){
        for (const kElement of this.keys) {
            if(kElement.match_key(k))return kElement.type === 'date';
        }
        return false;
    }

    DefaultDateField() {
        for (const k of this.keys) {
            if(k.type === 'date')return k.name;
        }
        throw `View: ${this.name} does not have a default date field`;
    }

    setDefaultSettings(){
        this.settings = [];
        for (const k of this.keys) {
            if(k.name==='id')this.settings.push(new Setting('id_display','ID'));
            else if(k.name==='performed_at')this.settings.push(new Setting('performed_at_display','Performed At'))
            else this.settings.push(new Setting(k.name,capitalize(k.name)));
        }
        return this;
    }

    transform_field(key, og) {
        for (const k of this.keys) {
            if(key === k.name){
                if(k.type==='date')throw `to_date('${og}','dd/mm/yyyy')`;
                if(k.type==='number')return Number(og);
                if(k.type==='string')return `'${og}'`;
            }
        }
    }
}
module.exports.RTable = RTable;
