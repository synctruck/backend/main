const {query} = require('../rredis');
const {current_alias,wrap_query,next_alias} = require("../rutils");
const {MODULE_ALIASES,VIEW_TABLES} = require("../rschema");

class Context{
    constructor() {
        this.usr = '';
        this.stmt = '';
        this.module = '';
        this.version = 1;
        this.anchor = 'r1';
        this.token = '';
    }
    getUser(){
        return this.usr;
    }
    update(module,version){
        if(this.module === module && this.version === version)return;
        module = module.toUpperCase();
        if(module in MODULE_ALIASES)module = MODULE_ALIASES[module];
        let aux = VIEW_TABLES[module];
        if(!aux)throw `Module: ${module} does not exist.`;
        if(!version)version = 1;
        aux = aux[version];
        if(!aux) throw `Invalid view table. Cannot find a view for module: ${module} version: ${version}`;

        this.module = module;
        this.version = version;

        this.stmt = `select * from ${/[a-zA-Z_0-9]+\s*\(\s*[0-9]+\s*\)/i.exec(this.stmt)[0]} ${this.anchor}`;

        const s = /[0-9]+/.exec(/\(\s*[0-9]+\s*\)/.exec(this.stmt)[0])[0];
        this.searchTable(`${VIEW_TABLES[this.module][this.version].name}(${s})`);

        query('hset',this.token,'module', this.module);
        query('hset',this.token,'version', this.version);
    }
    searchTable(table){
        this.anchor = 'r1';
        this.stmt = `select * from ${table} ${this.anchor}`;
        query('hset',this.token,'stmt',this.stmt);
        query('hset',this.token,'anchor',this.anchor);
    }
    reset(){
        if(this.stmt === '')return;
        this.anchor = 'r1';
        this.stmt = `select * from ${/[a-zA-Z_0-9]+\s*\(\s*[0-9]+\s*\)/i.exec(this.stmt)[0]} ${this.anchor}`;
        query('hset',this.token,'stmt',this.stmt);
        query('hset',this.token,'anchor',this.anchor);
    }
    wrap() { //Wraps current stmt & returns its anchor.
        this.anchor = next_alias(this.stmt);
        this.stmt = wrap_query(this.stmt,this.anchor);

        query('hset',this.token,'stmt',this.stmt);
        query('hset',this.token,'anchor',this.anchor);
    }
    filter(f){
        this.stmt = this.stmt + 'WHERE '+f+' ';
        query('hset',this.token,'stmt',this.stmt);
    }
    order (o){
        this.stmt = this.stmt + 'ORDER BY '+ o + ' ';
        query('hset',this.token,'stmt',this.stmt);
    }

    async connect (token){
        const r = await query('hgetall',`core:${token}`);
        if(!r) throw `Session validation failed.`;
        this.token = 'core:'+token;
        this.version = r.version;
        this.module = r.module;
        this.stmt = r.stmt;
        this.usr = r.usr;
        this.anchor = current_alias(this.stmt);
    }
    getModule(){return this.module;}
    getVersion(){return this.version;}
    getStmt(){return this.stmt;}
}

exports.Context = Context;
