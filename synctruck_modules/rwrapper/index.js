const dev = process.env.RMODE === 'dev';
function wrap(anotherFunction, ...rest) {
  return new Promise((resolve,reject)=>{
    anotherFunction(...rest).then(v=>{ 
        const jk = { 
          code: 200, 
          body: dev?JSON.stringify(v || null).length:JSON.stringify(v) 
        };
        if(dev)jk['command'] = [...rest];
        resolve( jk );
    }).catch((err) => {
          //Array of regex - codes to be associated with the message.
          //If no regex succeeds, 500 is returned.
          if(!dev)console.log(err); // do not log if in dev mode!
          let code = 500;
          const codes = [
              {
                  regex:/^.+denied.+$/i,
                  code: 403
              },
              {
                  regex:/^Parse\s+error/i,
                  code: 406
              }
          ];
          const m = (typeof err === "string")? err : err["message"] ? err["message"] : "Fatal. Internal server error."
          for (const c of codes){
              if(c.regex.test(m))code = c.code;
          }
        const jk = {code: code, body: JSON.stringify(m.toString()) };
        if (dev) jk['command'] = [...rest];
        resolve(jk);    
    });
  });
}
exports.wrap = wrap;
