/* Definición Léxica */
%lex

%options case-insensitive

%%

/* Special literals */
"'"([^"'"\\]|\\.)*"'"		                      		{return 'CHAR_STRING';}
"\""([^"\""\\]|\\.)*"\""		                    	{return 'STRING';}
[0-9]+th                                          {return 'INDEX';}
SYNC[0-9]+						                            {return 'SYNC_ID';}
[01]?[0-9]\/[0123]?[0-9]\/[0-9]{2}([0-9][0-9])?   {return 'DATE'}
[a-zA-Z]{3}\/[0123]?[0-9]\/[0-9]{2}([0-9][0-9])?   {return 'DATE_MONTH_FIRST'}
[0123]?[0-9]\/[a-zA-Z]{3}\/[0-9]{2}([0-9][0-9])?   {return 'DATE_MONTH_SECOND'}

/* Filter keywords */
"is"            						                {return 'IS';}
("null"|"unavailable"|"empty")\b	   				{return 'NULL';}
("includes"|"contains")\b	         					return 'CONTAINS';
"from"			            			         			return 'FROM';
"to"								                      	return 'TO';
"not"					              			          return 'NOT';
"and"         		                         	return 'AND';
"or"					                      		  	return 'OR';
"in"							                        	return 'IN';
"starts"						                        return 'STARTS';
"with"                                      return 'WITH';
"ends"				                        			return 'ENDS';
"desc"                                      return 'DESC';
"asc"                                       return 'ASC';
("row"|"rows")\b                            return 'ROW';
"valid"                                     return 'VALID';
"duplicate"                                 return 'DUPLICATE';
"invalid"                                   return 'INVALID';
"incomplete"                                return 'INCOMPLETE';
"changed"                       return "CHANGED";
"corrupt"                       return "CORRUPT";
"unchanged"                     return "UNCHANGED";
"complete"                      return "COMPLETE";
"reset"                         return "RESET";
enable\s+manual\s+mode\b        return "MANUAL";

/* Off limits keywords */
"search"					          		return 'SEARCH';
"filter"							        	return 'FILTER';
"order"								          return 'ORDER';
"by"								            return 'BY';
"preview"                       return 'PREVIEW';
"update"                        return 'UPDATE';
"edit"                          return 'EDIT';
"only"                          return 'ONLY';
"clone"                         return 'CLONE';
"prepare"                       return 'PREPARE';
"times"                         return "TIMES";
"current"                       return "CURRENT";
"set"                           return "SET";
"for"                           return "FOR";

/* Special Commands */

"[list-stations]"               return "C_LIST_STATIONS"
"[list-modules]"               return "C_LIST_MODULES"
"[update-station]"              return "C_UPDATE_STATION"
"[primary]"                     return "C_PRIMARY"
"[secondary]"                   return "C_SECONDARY"
"[change-applicant-status]"            return "C_APPLICANT";

"purge"  		            return "C_PURGE"
"[purge]"		            return "C_PURGE"

"cleanse"          return "C_CLEANSE"
"[cleanse]"          return "C_CLEANSE"

"fuse"              return "C_FUSE"
"[fuse]"              return "C_FUSE"

"[submit]"              return "C_SUBMIT"
"submit"              return "C_SUBMIT"

"[save]"              return "C_SAVE"
"save"              return "C_SAVE"

"commit"              return "C_COMMIT"
"[commit]"              return "C_COMMIT"

"[delete]"              return "C_DELETE"
"delete"              return "C_DELETE"

"[create]"              return "C_CREATE"
"create"              return "C_CREATE"


"display"             return "C_DISPLAY"
"[display]"             return "C_DISPLAY"

"[logbook]"               return "C_LOGBOOK"
"[fetch]"                   return "C_FETCH"
"[goto]"                    return "C_GOTO"
"[profile]"                    return "C_PROFILE"

/* Available modules: */
"help"                           return 'HELP';
("drivers"|"driver")\b           return 'DRIVER';
("users"|"user")\b               return 'USER';
("vans"|"van")\b                 return 'VAN';
("station"|"stations")\b         return 'STATION';
("applicant"|"applicants")\b         return 'APPLICANT';

/* Symbols & operators */
":"                               return 'COLON';
"("		              							return 'LEFT_PAREN';
")"							              		return 'RIGHT_PAREN';
"="						              			return 'IGUAL';
"!="				            					return 'DISTINTO';
">="							              	return 'MAYORIGUAL';
"<="					               			return 'MENORIGUAL';
">"					              				return 'MAYORQ';
"<"								              	return 'MENORQ';
","                                             return 'COMMA';
";"                                             return 'PUNTO_COMA';

/* Other */

([a-zA-Z]|_)+[0-9_A-Za-z]*        {return 'ID';}
(-)?[0-9]+("."[0-9]+)?		   			{return 'NUMBER';}
[ \r\t]+                                {/*WHITESPACE IGNORE*/}
\n                                      {/*NEW LINE. IGNORE*/}
<<EOF>>                 			           return 'EOF';
.										                    {throw new Error('Invalid character received.')}

/lex

/* Asociación de operadores y precedencia */
%left OR
%left AND
%left COMPARACION DISTINTO MAYORQ MENORQ MAYORIGUAL MENORIGUAL
%left UNOT

%start s_0

%% /* Definición de la gramática */

s_0 : command EOF {__root = $1;};

command: 	searchStmt {$$ = $1;}
		|   filterStmt {$$ = { type : 'filter', filter : $1} }
		|   filterStmt orderStmt {$$ = { type : 'filter', filter : $1, order : $2 } }
		|   orderStmt {$$ = { type : 'order', order : $1 } }
        |   previewStmt {$$ = $1;}
        |   editStmt {$$ = $1;}
        |   resetStmt {$$ = $1;}
        |   simplestCommand {$$ = $1;}
        |   buttonUpdateStmt {$$ = $1;}
        ;

simplestCommand: C_LIST_STATIONS { $$ = { type : Trim($1) } }
                | C_LIST_MODULES { $$ = { type : Trim($1) } }
                | C_PROFILE { $$ = { type : Trim($1) } }
                | C_CLEANSE { $$ = { type : Trim($1) } }
                | C_FUSE { $$ = { type : Trim($1) } }
                | C_PURGE { $$ = { type : Trim($1) } }
                | C_COMMIT { $$ = { type : Trim($1) } }
                | C_SAVE { $$ = { type : Trim($1) } }
                | C_SUBMIT { $$ = { type : Trim($1) } }
                | C_UPDATE_STATION ID { $$ = { type : Trim($1), station: $2 } }
                | C_FETCH NUMBER NUMBER { $$ = { type : Trim($1), limit: Number($2), offset: Number($3) } }
                | C_GOTO MODULE NUMBER { $$ = { type : Trim($1), module: $2, version: Number($3) } }
                | C_APPLICANT NUMBER { $$ = { type: 'change-applicant-status', id : Number($2) } }
                | C_LOGBOOK NUMBER { $$ = { type : Trim($1), id: Number($2) } }
                | C_DISPLAY NumericTarget { $$ = { type : Trim($1), id: $2 } }
                | C_CREATE { $$ = { type : 'display', id: 0 } }
                | C_DELETE NumericTargetL { $$ = { type : Trim($1), targets: $2 } }
                | C_DELETE NumericTargetL ROW { $$ = { type : Trim($1), targets: $2 } }
                | MANUAL NUMBER NUMBER { $$ = { type : 'manual-mode', limit: Number($2), offset: Number($3) } }
                ;

buttonUpdateStmt : C_PRIMARY StringL C_SECONDARY StringL { $$ = { type : 'update-buttons', primary : $2, secondary: $4 } }
                 ;

StringL : StringL COMMA PossibleString {$1.push($3); $$ = $1; }
            | PossibleString {$$ = [$1];}
            ;

PossibleString :  CHAR_STRING {$$ = (new Value('char_string',$1)).getValue(); }
                | STRING { $$ = (new Value('string',$1)).getValue();}
                ;

NumericTarget : NUMBER { $$ = Number($1); }
                |SYNC_ID {$$ = /[0-9]+/.exec($1)[0];}
                |INDEX {$$ = [/[0-9]+/.exec($1)[0]];}
                ;

previewStmt : PREVIEW MODULE {$$ = {type : 'preview', module: $2, version : 776} }
            | PREVIEW MODULE filterStmt {$$ = {type : 'preview', module: $2, filter:$3, version : 776 } ; }
            | PREVIEW MODULE orderStmt {$$ = {type : 'preview', module: $2, order:$3, version : 776 } ; }
            | PREVIEW MODULE filterStmt orderStmt {$$ = {type : 'preview', module: $2, filter:$3, order : $4, version : 776 }  }
            | PREVIEW {$$ = {type : 'preview', version : 776} ; }
            | PREVIEW filterStmt {$$ = {type : 'preview', filter : $2, version : 776} ; }
            | PREVIEW orderStmt {$$ = {type : 'preview', order : $2 , version : 776} ; }
            | PREVIEW filterStmt orderStmt {$$ = {type : 'preview', filter : $2, order : $3, version : 776} ; }
            | updateRequestL { $$ = { type: 'update-list', requests: $1 } }
            | CLONE indexL ROW NUMBER TIMES { $$ = {type : 'clone', targets : $2, times : $4}; }
            | PREPARE NUMBER ROW  {$$ = { type : 'prepare', times: $2, version : 776};}
            | PREVIEW MODULE PREPARE NUMBER ROW {$$ = { type : 'prepare', times: $4, module : $2, version : 776 }; }
            ;

updateRequestL : updateRequestL PUNTO_COMA updateRequest { $1.push($3); $$ = $1; }
                | updateRequest { $$ = [$1] }
                ;

updateRequest : UPDATE setClause FOR NumericTargetL ROW {$$ = {type : 'update',updates:$2,targets:$4}; }
            | UPDATE setClause FOR NumericTargetL  {$$ = {type : 'update',updates:$2,targets:$4}; }
            | UPDATE setClause FOR CURRENT SET {$$ = {type : 'update', updates:$2, targets:[] }; }
            ;

editStmt : EDIT MODULE {$$ = {type : 'edit', module : $2, version : 777 };}
          | EDIT MODULE filterStmt {$$ = {type : 'edit', module : $2, filter : $3, version : 777 }; }
          | EDIT MODULE filterStmt orderStmt {$$ = {type : 'edit', module : $2, filter : $3, order : $4, version : 777 }; }
          | EDIT MODULE orderStmt {$$ = {type : 'edit', module : $2, order : $3 , version : 777}; }
          | EDIT {$$ = {type : 'edit', version : 777} }
          | EDIT filterStmt {$$ = {type : 'edit', filter : $2, version : 777}; }
          | EDIT filterStmt orderStmt {$$ = {type : 'edit', filter : $2, order : $3, version : 777} ; }
          | EDIT orderStmt {$$ = {type : 'edit', order : $2, version : 777} ;}
          | EDIT NumericTargetL { $$ = { type : 'edit', version : 777, targets: $2 } }
          | EDIT NumericTargetL ROW { $$ = { type : 'edit', version : 777, targets: $2 } }
          | EDIT NumericTargetL ONLY { $$ = { type : 'edit', version : 777, targets: $2, only : true } }
          | EDIT NumericTargetL ROW ONLY { $$ = { type : 'edit', version : 777, targets: $2, only : true } }
          ;
indexL : indexL COMMA INDEX {$1.push(/[0-9]+/.exec($3)[0]); $$ = $1;}
            | INDEX {$$ = [/[0-9]+/.exec($1)[0]];}
            ;

 NumericTargetL : NumericTargetL COMMA NumericTarget {$1.push($3); $$ = $1;}
            | NumericTarget {$$ = [$1];}
            ;

setClause : setClause COMMA FilterKey IGUAL setAtomic {$1.push({key:$3.getValue(),value:$5}); $$ = $1;}
            | FilterKey IGUAL setAtomic {$$ = [{key:$1.getValue(),value:$3}];}
            ;

setAtomic : NUMBER {$$ = $1; }
            		| CHAR_STRING {$$ = $1.substring(1,$1.length-1); }
            		| STRING {$$ = $1.substring(1,$1.length-1);}
            		| DATE  {$$ = $1 }
            		| EMAIL {$$ = $1 }
            		| SYNC_ID {$$ = /[0-9]+/.exec($1)[0];}
            		;
resetStmt : RESET filterStmt orderStmt { $$ = {type :'reset', filter : $2, order : $3}; }
            | RESET filterStmt { $$ = {type :'reset', filter : $2 }; }
            | RESET orderStmt { $$ = {type :'reset', order : $2 }; }
            | RESET orderStmt filterStmt { $$ = {type :'reset', filter : $3, order : $2}; }
            | RESET { $$ = {type :'reset'}; }
            ;

filterStmt : FILTER Exp {$$ = $2}
            ;

orderStmt : ORDER BY KeyL {$$ = {fields:$3,direction:"ASC"};}
            | ORDER BY KeyL DESC {$$ = {fields:$3,direction:"DESC"};}
            | ORDER BY KeyL ASC {$$ = {fields:$3,direction:"ASC"};}
            ;

searchStmt : SEARCH Key filterStmt orderStmt
{
                  $$ = { type : 'search', module: $2.getModule(), version: $2.getVersion(),
                         filter : $3, order : $4 }
}
            | SEARCH Key filterStmt
{
                  $$ = { type : 'search', module: $2.getModule(), version: $2.getVersion(),
                         filter : $3 }
}
            | SEARCH Key orderStmt
{
                  $$ = { type : 'search', module: $2.getModule(), version: $2.getVersion(),
                         order : $3 }
}
            | SEARCH Key
{
                  $$ = { type : 'search', module: $2.getModule(), version: $2.getVersion() }
}
            | SEARCH Key FROM date_literal TO date_literal orderStmt
{
                  $$ = {  type : 'search',
                          module: $2.getModule(),
                          version: $2.getVersion(),
                          filter : new Filter("contains",null,[$4.getValue(),$6.getValue()]),
                          order : $7
                       }
}
            | SEARCH Key FROM date_literal orderStmt
{
                  $$ = { type : 'search',
                                           module: $2.getModule(),
                                           version: $2.getVersion(),
                                           filter : new Filter("contains",null,$4.getValue()),
                                           order : $5
                                         }
}
            | SEARCH Key FROM date_literal TO date_literal
                {
                  $$ = {
                                         type : 'search',
                                           module: $2.getModule(),
                                           version: $2.getVersion(),
                                           filter : new Filter("contains",null,[$4.getValue(),$6.getValue()])
                                         }
                }
            | SEARCH Key FROM date_literal
{
              $$ = {
                                 type : 'search',
                                   module: $2.getModule(),
                                   version: $2.getVersion(),
                                   filter : new Filter("from",null,$4.getValue())
                                 }
}
            | SEARCH Key CONTAINS text_literal orderStmt {
            $$ = {
                 type : 'search',
                               module: $2.getModule(),
                               version: $2.getVersion(),
                               filter : new Filter("contains",null,$4.getValue()),
                               order : $5
                 }
            }
            | SEARCH Key CONTAINS text_literal {$$ = {
                                                                 type : 'search',
                                                                   module: $2.getModule(),
                                                                   version: $2.getVersion(),
                                                                   filter : new Filter("contains",null,$4.getValue())
                                                                 }
                                                 }
            ;

SyncL : SyncL COMMA id_literal {$1.push($3.getValue()); $$ = $1;}
        | id_literal {$$ = [$1.getValue()];}
        ;

NumL : NumL COMMA NUMBER {$1.push(Number($3)); $$ = $1;}
        | NUMBER {$$ = [Number($1)];}
        ;

IdList : IdList COMMA ID {$1.push($3); $$ = $1;}
        | ID {$$ = [$1];}
        ;

MODULE : DRIVER {$$ = "DRIVER";}
        | VAN {$$ = "VAN";}
        | USER {$$ = "USER";}
        | STATION {$$ = "STATION"}
        | APPLICANT {$$ = "APPLICANT"}
        | HELP {$$ = "HELP"}
        ;

Key : Key KeyUnit {$1.push_key($2); $$ = $1;}
    | KeyUnit {$$ = new Key($1); }
    ;

KeyL : KeyL COMMA FilterKey {$1.push($3.getValue()); $$ = $1;}
      | FilterKey {$$ = [$1.getValue()];}
      ;

KeyUnit : ID {$$ = $1;}
          | DRIVER {$$ = $1}
          | USER {$$ = $1}
          | VAN {$$ = $1}
          | STATION {$$ = $1}
          | HELP {$$ = $1}
          | APPLICANT {$$ = $1}
          ;

FilterKey : Key {$$ = $1;}
          ;

Exp : InferredKeys {$$ = $1;}
    | FilterKey FROM date_literal TO date_literal {$$ = new Filter("from_to",$1,[$3.getValue(),$5.getValue()])}
    | FilterKey FROM date_literal {$$ = new Filter("from",$1,$3.getValue())}
    | FilterKey STARTS WITH text_literal {$$ = new Filter("starts_with",$1,$4.getValue())}
    | FilterKey ENDS WITH text_literal {$$ = new Filter("ends_with",$1,$4.getValue())}
    | FilterKey CONTAINS text_literal {$$ = new Filter("contains",$1,$3.getValue())}
		| FilterKey MAYORQ atomic {$$ = new Filter(">",$1,$3.getValue())}
		| FilterKey MENORQ atomic {$$ = new Filter("<",$1,$3.getValue())}
		| FilterKey MAYORIGUAL atomic {$$ = new Filter(">=",$1,$3.getValue())}
		| FilterKey MENORIGUAL atomic {$$ = new Filter("<=",$1,$3.getValue())}
		| FilterKey IGUAL atomic {$$ = new Filter("=",$1,$3.getValue())}
		| FilterKey DISTINTO atomic {$$ = new Filter("!=",$1,$3.getValue())}
		| Exp AND Exp {$$ = new Filter("and",null,null,$1,$3);}
		| Exp OR Exp {$$ = new Filter("or",null,null,$1,$3);}
    | NOT Exp %prec UNOT {$$ = new Filter("not",null,null,$2,$2); }
		| LEFT_PAREN Exp RIGHT_PAREN {$$ = $2;}
		| FilterKey IN LEFT_PAREN atomList RIGHT_PAREN {$$ = new Filter("in",$1,$4); }
		| FilterKey NOT IN LEFT_PAREN atomList RIGHT_PAREN {$$ = new Filter("not_in",$1,$5); }
		| FilterKey IS NULL {$$ = new Filter("is_null",$1,null); }
		| FilterKey IS NOT NULL {$$ = new Filter("is_not_null",$1,null); }
		| ROW IS VALID {$$ = new Filter("=",new Key('color'),'green');}
		| ROW IS DUPLICATE {$$ = new Filter("=",new Key('color'),'yellow');}
		| ROW IS INVALID {$$ = new Filter("=",new Key('color'),'red');}
		| ROW IS INCOMPLETE {$$ = new Filter("=",new Key('color'),'orange');}
		| ROW IS UNCHANGED {$$ = new Filter("=",new Key('color'),'purple');}
		| ROW IS CORRUPT {$$ = new Filter("=",new Key('color'),'brown');}
		| ROW IS NOT VALID {$$ = new Filter("!=",new Key('color'),'green');}
		| ROW IS NOT DUPLICATE {$$ = new Filter("!=",new Key('color'),'yellow');}
		| ROW IS NOT INVALID {$$ = new Filter("!=",new Key('color'),'red');}
		| ROW IS COMPLETE {$$ = new Filter("!=",new Key('color'),'orange');}
		| ROW IS CHANGED {$$ = new Filter("=",new Key('color'),'purple');}
		| ROW IS NOT CORRUPT {$$ = new Filter("=",new Key('color'),'brown');}
		;

InferredKeys: FROM date_literal { $$ = new Filter("from",null,$2.getValue()) }
              | FROM date_literal TO date_literal { $$ = new Filter("from_to",null,[$2.getValue(),$4.getValue()]) }
              | CONTAINS text_literal { $$ = new Filter("contains",null,$2.getValue()); }
              | STARTS WITH text_literal {$$ = new Filter("starts_with",null,$3.getValue());}
              | ENDS WITH text_literal {$$ = new Filter("ends_with",null,$3.getValue());}
              ;

atomList : atomList COMMA atomic { $1.push($3.getValue()); $$ = $1;}
       | atomic {$$ = [$1.getValue()];}
       ;

id_literal : SYNC_ID {$$ = new Value('id',$1);}
            ;

number_literal : NUMBER {$$ = new Value('number',$1);}
                ;

date_literal : DATE  {$$ = new Value('date',$1); }
          		| DATE_MONTH_FIRST  {$$ = new Value('date_month_first',$1); }
		          | DATE_MONTH_SECOND  {$$ = new Value('date_month_second',$1); }
              ;

text_literal : EMAIL {$$ = new Value('email',$1);}
    | CHAR_STRING {$$ = new Value('char_string',$1); }
    | STRING {$$ = new Value('string',$1);}
    ;

atomic : NUMBER {$$ = new Value('number',$1); }
		| CHAR_STRING {$$ = new Value('char_string',$1); }
		| STRING {$$ = new Value('string',$1);}
		| SYNC_ID {$$ = new Value('id',$1);}
		| DATE  {$$ = new Value('date',$1); }
		| DATE_MONTH_FIRST  {$$ = new Value('date_month_first',$1); }
		| DATE_MONTH_SECOND  {$$ = new Value('date_month_second',$1); }
		| EMAIL {$$ = new Value('email',$1);}
		;
