const { MONTH_LITERALS, TABLE_MAPS } = require('../rschema');

class Filter{
    constructor(op,key,value,left,right) {
        this.op = op;
        if(left && right){
        this.left = left;
        this.right = right;
    } else if (left && !right) {
        this.left = left;
        this.right = left;
    } else{
            if(key)this.key = key.getValue();
            this.value = value;
        }
    }
}

class Key{
    constructor(k) {
        this.val = '';
        this.values = [k.toLowerCase()];
    }
    push_key(k){
        this.values.push(k.toLowerCase());
    }
    getVersion(){
        this.val = this.getValue();
        if(!(this.val in TABLE_MAPS))throw `Table: ${this.val.replace('_',' ')} does not exist.`;
        return TABLE_MAPS[this.val].version;
    }
    getModule(){
        this.val = this.getValue();
        //Check if val is an actual valid table key. If not throw.
        if(!(this.val in TABLE_MAPS))throw `Table: ${this.val.replace('_',' ')} does not exist.`;
        return TABLE_MAPS[this.val].module;
    }
    getValue(){
        return this.values.join('_');
    }
}
class Value{
    constructor(type,value) {
        this.type = type;
        switch (this.type) {
            case 'id':this.val = Number(/[0-9]+/.exec(value)[0]); break;
            case 'date':this.val = `to_date('${value}','mm/dd/yyyy')`;break;
            case 'date_month_first':
            {
                value = value.replace(/[a-zA-Z]+/,(m)=>{
                    m = m.toLowerCase();
                    if(!(m in MONTH_LITERALS))throw `Invalid month: ${m}. Refer to value documentation for further details.`;
                    return MONTH_LITERALS[m].toString();
                });
                this.val = `to_date('${value}','mm/dd/yyyy')`;
            }
                break;
            case 'date_month_second':{
                value = value.replace(/[a-zA-Z]+/,(m)=>{
                    m = m.toLowerCase();
                    if(!(m in MONTH_LITERALS))throw `Invalid month: ${m}. Refer to value documentation for further details.`;
                    return MONTH_LITERALS[m].toString();
                });
                this.val = `to_date('${value}','dd/mm/yyyy')`;
            }break;
            case 'string':
            case 'char_string':
                this.val = value.substring(1,value.length-1);break; //We remove the escaped quotes.
            case 'number': this.val = Number(value);break;
            default:this.val = value; //Emails or Numbers can be sent directly.
        }
    }
    getValue(){
        return this.val;
    }
}

function Trim(str){
    if(str[0]==='[') return str.substring(1,str.length-1).toLowerCase();
    else return str.trim().toLowerCase();
}

module.exports.Trim = Trim;
module.exports.Filter = Filter;
module.exports.Key = Key;
module.exports.Value = Value;
